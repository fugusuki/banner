import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;

import javax.xml.stream.XMLStreamException;

import com.sun.tools.javac.util.*;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import dragon.nlp.tool.Tagger;
import dragon.nlp.tool.lemmatiser.EngLemmatiser;

import banner.eval.BANNER;
import banner.postprocessing.PostProcessor;
import banner.tagging.CRFTagger;
import banner.tagging.dictionary.DictionaryTagger;
import banner.tokenization.Tokenizer;
import banner.types.Mention;
import banner.types.Sentence;
import banner.util.SentenceBreaker;

import bioc.BioCAnnotation;
import bioc.BioCCollection;
import bioc.BioCDocument;
import bioc.BioCPassage;
import bioc.io.BioCDocumentWriter;
import bioc.io.BioCFactory;
import bioc.io.woodstox.ConnectorWoodstox;

// ---------------- TopCoder submission generation ----------------
import bioc.BioCLocation;
import java.io.PrintWriter;
import java.io.*;
import java.util.List;
// ----------------------------------------------------------------

public class BANNER_BioC {

	private SentenceBreaker breaker;
	public ArrayList<CRFTagger> taggers = new ArrayList<CRFTagger>();
	private Tokenizer tokenizer;
	private PostProcessor postProcessor;

	public static void main(String[] args) throws IOException, XMLStreamException, ConfigurationException, InterruptedException {
        BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", 0, 840000);
        bannerBioC.processFile("data/test_file.xml", 8, "0_840000");

        /*
        int scoreIndex;
        scoreIndex = 1;
        for (int minScore = 880000; minScore <= 900000; minScore += 10000) {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", scoreIndex, minScore);
            for (int i = 1; i <= 4; i++) {
                if(bannerBioC.taggers.size() * i / 10 == bannerBioC.taggers.size() * (i-1) / 10) continue;
                bannerBioC.processFile("data/test_file.xml", bannerBioC.taggers.size() * i / 10, scoreIndex + "_" + minScore);
            }
        }
        scoreIndex = 2;
        for (int minScore = 810000; minScore <= 850000; minScore += 20000) {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", scoreIndex, minScore);
            for (int i = 1; i <= 5; i++) {
                if(bannerBioC.taggers.size() * i / 10 == bannerBioC.taggers.size() * (i-1) / 10) continue;
                bannerBioC.processFile("data/test_file.xml", bannerBioC.taggers.size() * i / 10, scoreIndex + "_" + minScore);
            }
        }
        scoreIndex = 0;
        for (int minScore = 830000; minScore <= 860000; minScore += 10000) {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", scoreIndex, minScore);
            for (int i = 1; i <= 6; i++) {
                if(bannerBioC.taggers.size() * i / 10 == bannerBioC.taggers.size() * (i-1) / 10) continue;
                bannerBioC.processFile("data/test_file.xml", bannerBioC.taggers.size() * i / 10, scoreIndex + "_" + minScore);
            }
        }
        {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", 0, 830000);
            bannerBioC.processFile("data/test_file.xml", 9, "0_830000");
        }

        {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", 0, 830000);
            bannerBioC.processFile("data/test_file.xml", 13, "0_830000");
        }
        {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", 0, 840000);
            bannerBioC.processFile("data/test_file.xml", 7, "0_840000");
        }
        {
            BANNER_BioC bannerBioC = new BANNER_BioC("config/banner_bioc.xml", 0, 840000);
            bannerBioC.processFile("data/test_file.xml", 8, "0_840000");
        }
        */
    }

	public BANNER_BioC(String configFilename, int scoreIndex, int minScore) throws IOException, ConfigurationException {
		//called with parameters(which score is used as criterion of filtering, minimum score value), tagger files whose score meets the condition of the parameters are loaded and a list of taggers is created.

        HierarchicalConfiguration config = new XMLConfiguration(configFilename);
        DictionaryTagger dictionary = BANNER.getDictionary(config);
        EngLemmatiser lemmatiser = BANNER.getLemmatiser(config);
        tokenizer = BANNER.getTokenizer(config);
		Tagger posTagger = BANNER.getPosTagger(config);
		postProcessor = BANNER.getPostProcessor(config);
        breaker = new SentenceBreaker();
        for(File f : new File("tagger").listFiles()){
            String[] splittedName = f.getName().split("\\.");
            if(splittedName.length <= 1 || !splittedName[1].equalsIgnoreCase("tagger")) continue;
            if(Integer.parseInt(splittedName[0].split("_")[scoreIndex]) < minScore) continue;

            CRFTagger tagger = CRFTagger.load(f, lemmatiser, posTagger, dictionary);
            taggers.add(tagger);
        }
	}

	private void processFile(String inXML, int minCount, String fileNameSuffix) throws IOException, XMLStreamException {
		//for each sentence in testing file is processed by processPassage method and submission file is created.

        ConnectorWoodstox connector = new ConnectorWoodstox();
        connector.startRead(new InputStreamReader(new FileInputStream(inXML), "UTF-8"));

		// ---------------- TopCoder submission generation ----------------
		PrintWriter submission = new PrintWriter(new FileWriter("BannerAnnotate_" + fileNameSuffix + "_" + minCount + ".java.txt"));
		submission.println("public class BannerAnnotate {");
		int numArray = 1;
		int numItems = 0;
        submission.println("static void init0() {");
        submission.println("a0 = new int[] {");
		// ----------------------------------------------------------------
				
		while (connector.hasNext()) {
			BioCDocument document = connector.next();
			String documentId = document.getID();
			System.out.println("ID=" + documentId);
			for (BioCPassage passage : document.getPassages()) {
				processPassage(documentId, passage, minCount);
			}
			System.out.println();

		// ---------------- TopCoder submission generation ----------------
			for (BioCPassage passage : document.getPassages()) {
	            for (BioCAnnotation annotation : passage.getAnnotations()) {
	            
	                String str = document.getID();
	                for (BioCLocation loc : annotation.getLocations()) {
	                    str += "," + loc.getOffset() + "," + loc.getLength() + ",";
	                }
	                if (annotation.getLocations().size()>0) {
		                submission.println(str);
		                numItems++;
		                if ((numItems%1000)==0) {
		                    submission.println("};");
		                    submission.println("}");
                            submission.println("static void init"+numArray+"() {");
                            submission.println("a"+numArray+" = new int[] {");
		                    numArray++;
		                }
		            }
	            }				    
			}
		// ----------------------------------------------------------------

		}

		// ---------------- TopCoder submission generation ----------------
		submission.println("};");
		submission.println("}");
		for (int i=0;i<numArray;i++) 
		{
    		submission.println("static int[] a"+i+";");
		}
		submission.println("static {");
		for (int i=0;i<numArray;i++) 
		{
    		submission.println("init"+i+"();");
		}
		submission.println("}");
  		submission.println("int[] annotate() {");
        submission.println("int[] ans = new int["+(numItems*3)+"];");
        submission.println("int idx = 0;");
        for (int i=0;i<numArray;i++) {
            submission.println("for (int i:a"+i+") ans[idx++] = i;");
        }
  		submission.println("return ans; }}");
		submission.flush();
        submission.close();
		// ----------------------------------------------------------------
	}

	private void processPassage(String documentId, BioCPassage passage, int minCount) {
        //called with parameter(threshold of vote), each tagger in list of taggers makes annotations for the passage and returns result by using simple vote process to the annotations.

        // Figure out the correct next annotation ID to use
		int nextId = 0;
		for (BioCAnnotation annotation : passage.getAnnotations()) {
			String annotationIdString = annotation.getID();
			if (annotationIdString.matches("[0-9]+")) {
				int annotationId = Integer.parseInt(annotationIdString);
				if (annotationId > nextId)
					nextId = annotationId;
			}
		}

		// Process the passage text
		System.out.println("Text=" + passage.getText());
		breaker.setText(passage.getText());
		int offset = passage.getOffset();
		List<String> sentences = breaker.getSentences();
        for (int i = 0; i < sentences.size(); i++) {
			String sentenceText = sentences.get(i);
			String sentenceId = Integer.toString(i);
			if (sentenceId.length() < 2)
				sentenceId = "0" + sentenceId;
			sentenceId = documentId + "-" + sentenceId;
			Sentence sentence = new Sentence(sentenceId, documentId, sentenceText);

            // generate all mentions from taggers
            Map<Mention, Integer> answers = new HashMap<Mention, Integer>();
            for (CRFTagger tagger : taggers) {
                Sentence sentence2 = BANNER.process(tagger, tokenizer, postProcessor, sentence);
                for (Mention mention : sentence2.getMentions()) {
                    Integer value = answers.get(mention);
                    if (value == null) value = 0;
                    answers.put(mention, value + 1);
                }
            }

            // select the answers by simple vote method
            ArrayList<Map.Entry<Mention, Integer>> answersList = new ArrayList<Map.Entry<Mention, Integer>>(answers.entrySet());
            Collections.sort(answersList, new Comparator<Map.Entry<Mention, Integer>>() {
                @Override
                public int compare(Map.Entry<Mention, Integer> o1, Map.Entry<Mention, Integer> o2) {
                    return o2.getValue() - o1.getValue();
                }
            });
            List<Mention> mentions = new ArrayList<Mention>();
            for (Map.Entry<Mention, Integer> answer : answersList) {
                if (answer.getValue() < minCount) break;
                Mention mention = answer.getKey();
                boolean flg = true;
                for(Mention m : mentions){
                    if(mention.overlaps(m)) {
                        flg = false;
                        break;
                    }
                }
                if(flg) mentions.add(mention);
            }
			for (Mention mention : mentions) {
				BioCAnnotation annotation = new BioCAnnotation();
				nextId++;
				annotation.setID(Integer.toString(nextId));
				String entityType = mention.getEntityType().getText();
				if (entityType.matches("[A-Z]+")) {
					entityType = entityType.toLowerCase();
					String first = entityType.substring(0, 1);
					entityType = entityType.replaceFirst(first, first.toUpperCase());
				}
				annotation.setInfons(Collections.singletonMap("type", entityType));
				String mentionText = mention.getText();
				annotation.setLocation(offset + mention.getStartChar(), mentionText.length());
				annotation.setText(mentionText);
				passage.addAnnotation(annotation);
			}
			offset += sentenceText.length();
		}
	}
}
