package banner.eval;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import banner.eval.dataset.Dataset;
import banner.postprocessing.FlattenPostProcessor;
import banner.postprocessing.LocalAbbreviationPostProcessor;
import banner.postprocessing.ParenthesisPostProcessor;
import banner.postprocessing.PostProcessor;
import banner.postprocessing.SequentialPostProcessor;
import banner.postprocessing.FlattenPostProcessor.FlattenType;
import banner.tagging.CRFTagger;
import banner.tagging.FeatureSet;
import banner.tagging.TagFormat;
import banner.tagging.dictionary.DictionaryTagger;
import banner.tokenization.Tokenizer;
import banner.types.EntityType;
import banner.types.Mention;
import banner.types.Sentence;
import banner.types.Token;
import banner.types.Mention.MentionType;
import banner.types.Sentence.OverlapOption;
import banner.util.CollectionsRand;
import banner.util.RankedList;

import dragon.nlp.tool.HeppleTagger;
import dragon.nlp.tool.MedPostTagger;
import dragon.nlp.tool.Tagger;
import dragon.nlp.tool.lemmatiser.EngLemmatiser;

public class BANNER {

	public static void main(String[] args) throws ConfigurationException, IOException, InterruptedException {
        train();
	}

    public static ConcurrentHashMap<CRFTagger, Integer> train() throws ConfigurationException, IOException, InterruptedException{
        HierarchicalConfiguration config = new XMLConfiguration("config/banner_bioc.xml");
        HierarchicalConfiguration dataSetLocalConfig = config.configurationAt(Dataset.class.getPackage().getName(), true);

        dataSetLocalConfig.setProperty("filename", "data/ncbi_train_bioc.xml");
        Dataset datasetEx = getDataset(config);
        Set<Sentence> sentencesExAll = datasetEx.getSentences();

        Random rnd = new Random(0);  //Random rnd = new Random(1)   //first 33 files are created by seed 0. from 34th file, files are created by seed 0
        final ConcurrentHashMap<CRFTagger, Integer> taggers = new ConcurrentHashMap<CRFTagger, Integer>();
        List<Thread> threads = new ArrayList<Thread>();
        for(File f : new File("train").listFiles()){
            if(threads.size() >= 3){
                for(int i = 0; i< threads.size() ;i++) threads.get(i).start();
                for(int i = 0; i< threads.size() ;i++) threads.get(i).join();
                threads.clear();
            }

            String[] splittedName = f.getName().split("\\.");
            if(splittedName.length <= 1 || !splittedName[1].equalsIgnoreCase("xml")) continue;
            final String name = splittedName[0];

            // the training xml files created by TestAggregation.java are read.
            dataSetLocalConfig.setProperty("filename", "train/" + name + ".xml");
            Dataset datasetMt = getDataset(config);

            // Each file is concatenated with the training set that is randomly sampled from "config/banner_bioc.xml" and used for creating tagger by createTagger method.
            List<HashSet<Sentence>> sentencesExSplit = myRandomSplit(sentencesExAll, 0.8, rnd);
            final Set<Sentence> sentencesEx = sentencesExSplit.get(0);
            final Set<Sentence> sentencesExOOB = sentencesExSplit.get(1);
            final Set<Sentence> sentencesMt = myRandomSplit(datasetMt.getSentences(), 1.0, rnd).get(0);

            threads.add(new Thread(){
                @Override
                public void run() {
                    try {
                        AbstractMap.SimpleEntry<CRFTagger, Integer> result = createTagger(name, sentencesMt, sentencesEx, sentencesExOOB);
                        taggers.put(result.getKey(), result.getValue());
                    }
                    catch(Exception e){ }
                }
            });
        }
        for(int i = 0; i< threads.size() ;i++) threads.get(i).start();
        for(int i = 0; i< threads.size() ;i++) threads.get(i).join();

        return taggers;
    }

    public static List<HashSet<Sentence>> myRandomSplit(Set<Sentence> set, double subsetPercentage, Random rnd){
        HashSet<Sentence> set1 = new HashSet<Sentence>();
        HashSet<Sentence> set2 = new HashSet<Sentence>();
        String documentId = "";
        boolean flg = false;
        for (Sentence s : set){
            if(!s.getDocumentId().equals(documentId)) flg = rnd.nextDouble() < subsetPercentage;
            (flg ? set1 : set2).add(s);
        }
        return Arrays.asList(set1, set2);
    }

    public static AbstractMap.SimpleEntry<CRFTagger, Integer> createTagger(String name, Set<Sentence> sentencesMt, Set<Sentence> sentencesEx, Set<Sentence> sentencesExOOB) throws ConfigurationException, IOException{
        //tagger is created by CRFTagger.train method and tested with out of bag sample. Then each tagger is saved as a file whose filename contains the score(all of precision, recall, f-score) of the out of bag test.

        HierarchicalConfiguration config = new XMLConfiguration("config/banner_bioc.xml");
        sentencesMt.addAll(sentencesEx);

        TagFormat tagFormat = getTagFormat(config);
        Tokenizer tokenizer = getTokenizer(config);
        PostProcessor postProcessor = getPostProcessor(config);
        DictionaryTagger dictionary = getDictionary(config);
        int crfOrder = getCRFOrder(config);
        EngLemmatiser lemmatiser = getLemmatiser(config);
        Tagger posTagger = getPosTagger(config);
        Set<MentionType> mentionTypes = getMentionTypes(config);
        OverlapOption sameTypeOverlapOption = getSameTypeOverlapOption(config);
        OverlapOption differentTypeOverlapOption = getDifferentTypeOverlapOption(config);

        String simFindFilename = getSimFindFilename(config);
        FeatureSet featureSet = new FeatureSet(tagFormat, lemmatiser, posTagger, dictionary, simFindFilename, mentionTypes, sameTypeOverlapOption, differentTypeOverlapOption);
        CRFTagger tagger = CRFTagger.train(sentencesMt, crfOrder, tagFormat, featureSet);

        //test the performance of the tagger
        Performance performance = new Performance(MatchCriteria.Strict);
        for (Sentence sentence : sentencesExOOB) {
            Sentence sentence2 = process(tagger, tokenizer, postProcessor, sentence);
            performance.update(sentence, sentence2);
        }

        performance.print();
        Integer score = Integer.valueOf((int) (performance.getOverall().getFMeasure() * 1000000));
        int precision = (int)(performance.getOverall().getPrecision() * 1000000);
        int recall = (int)(performance.getOverall().getRecall() * 1000000);
        tagger.write(new File("tagger/" + score + "_" + precision + "_" + recall + "_" + name + ".tagger"));

        return new AbstractMap.SimpleEntry<CRFTagger, Integer>(tagger, score);
    }

	public static void logInput(Set<Sentence> sentences, HierarchicalConfiguration config) throws IOException {
		logInput(sentences, config, null);
	}

	private static void logInput(Set<Sentence> sentences, HierarchicalConfiguration config, String filenameSuffix) throws IOException {
		// TODO Handle nulls
		TagFormat tagFormat = getTagFormat(config);
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String idInputFilename = getFilename(localConfig.getString("idInputFilename"), filenameSuffix);
		String rawInputFilename = getFilename(localConfig.getString("rawInputFilename"), filenameSuffix);
		String trainingInputFilename = getFilename(localConfig.getString("trainingInputFilename"), filenameSuffix);
		PrintWriter idFile = new PrintWriter(new BufferedWriter(new FileWriter(idInputFilename)));
		PrintWriter rawFile = new PrintWriter(new BufferedWriter(new FileWriter(rawInputFilename)));
		PrintWriter trainingFile = new PrintWriter(new BufferedWriter(new FileWriter(trainingInputFilename)));
		for (Sentence sentence : sentences) {
			idFile.println(sentence.getSentenceId());
			rawFile.println(sentence.getText());
			trainingFile.println(getTrainingText(sentence, tagFormat, EnumSet.of(MentionType.Required), OverlapOption.Raw, OverlapOption.Raw));
		}
		idFile.close();
		rawFile.close();
		trainingFile.close();
	}

	public static String getTrainingText(Sentence sentence, TagFormat format, Set<MentionType> mentionTypes, OverlapOption sameType, OverlapOption differentType) {
		StringBuilder trainingText = new StringBuilder();
		List<String> labels = sentence.getTokenLabels(format, mentionTypes, sameType, differentType);
		List<Token> tokens = sentence.getTokens();
		for (int i = 0; i < tokens.size(); i++) {
			Token token = tokens.get(i);
			trainingText.append(token.getText());
			trainingText.append("|");
			trainingText.append(labels.get(i));
			trainingText.append(" ");
		}
		return trainingText.toString().trim();
	}

	private static String getFilename(String originalFilename, String filenameSuffix) {
		if (originalFilename == null)
			return null;
		if (filenameSuffix == null)
			return originalFilename;
		int period = originalFilename.lastIndexOf(".");
		String name = originalFilename;
		String extension = "";
		if (period != -1) {
			name = originalFilename.substring(0, period);
			extension = originalFilename.substring(period);
		}
		return name + filenameSuffix + extension;
	}

	private static void outputMentions(Sentence sentence, PrintWriter mentionOutputFile, boolean onlyNonBlank, boolean ignoreWhitespace) {
		// FIXME What is onlyNonBlank for???
		if (onlyNonBlank) {
			List<Token> tokens = sentence.getTokens();
			int charCount = 0;
			for (int i = 0; i < tokens.size(); i++) {
				List<Mention> mentions = sentence.getMentions(tokens.get(i), EnumSet.of(MentionType.Found));
				assert mentions.size() == 0 || mentions.size() == 1;
				Mention mention = null;
				if (mentions.size() > 0)
					mention = mentions.get(0);
				if (mention != null && i == mention.getStart()) {
					mentionOutputFile.print(sentence.getSentenceId());
					mentionOutputFile.print("|");
					mentionOutputFile.print(charCount);
					mentionOutputFile.print(" ");
				}
				charCount += tokens.get(i).length();
				if (mention != null && i == mention.getEnd() - 1) {
					mentionOutputFile.print(charCount - 1);
					mentionOutputFile.print("|");
					mentionOutputFile.println(mention.getText());
				}
			}
		} else {
			for (Mention mention : sentence.getMentions(MentionType.Found)) {
				mentionOutputFile.print(sentence.getSentenceId());
				mentionOutputFile.print("|");
				mentionOutputFile.print(mention.getStartChar(ignoreWhitespace));
				mentionOutputFile.print(" ");
				mentionOutputFile.print(mention.getEndChar(ignoreWhitespace));
				mentionOutputFile.print("|");
				mentionOutputFile.println(mention.getText());
			}
		}
	}

	public static Sentence process(banner.tagging.Tagger tagger, Tokenizer tokenizer, PostProcessor postProcessor, Sentence sentence) {
		// TODO Solidify ability to separate found/required/allowed mentions
		// TODO Then use original Sentence for tagging instead of copy
		// TODO Can we make tokenization and post-processing part of the MALLET
		// pipe?
		Sentence sentence2 = sentence.copy(false, false);
		tokenizer.tokenize(sentence2);
		tagger.tag(sentence2);
		postProcessor.postProcess(sentence2);
		return sentence2;
	}

	private enum FontColor {
		Black, Blue, Green, Red, Purple;

		@Override
		public String toString() {
			return name().toLowerCase();
		}

		public String changeColor(FontColor newColor) {
			StringBuffer str = new StringBuffer();
			if (!equals(newColor) && !equals(Black))
				str.append("</font>");
			str.append(" ");
			if (!equals(newColor) && !newColor.equals(Black))
				str.append("<font color=\"" + newColor.toString() + "\">");
			return str.toString();
		}
	}

	public enum MatchCriteria {
		Strict, Left, Right, LeftOrRight, Approximate, Partial;
	}

	public static class Performance {

		private PerformanceData overall;
		private Map<EntityType, PerformanceData> perMention;
		private Map<String, PerformanceData> perText;

		public Performance(MatchCriteria matchCriteria) {
			// TODO Implement
			if (matchCriteria != MatchCriteria.Strict)
				throw new IllegalArgumentException("Not implemented");
			overall = new PerformanceData();
			perMention = new HashMap<EntityType, PerformanceData>();
			perText = new HashMap<String, PerformanceData>();
		}

		private PerformanceData getMentionPerformanceData(EntityType type) {
			PerformanceData performanceData = perMention.get(type);
			if (performanceData == null) {
				performanceData = new PerformanceData();
				perMention.put(type, performanceData);
			}
			return performanceData;
		}

		private PerformanceData getTextPerformanceData(String text) {
			PerformanceData performanceData = perText.get(text);
			if (performanceData == null) {
				performanceData = new PerformanceData();
				perText.put(text, performanceData);
			}
			return performanceData;
		}

		public void update(Sentence sentenceRequired, Sentence sentenceFound) {
			// TODO Write these TP/*TP/FP/FN to file
			Set<Mention> mentionsNotFound = new HashSet<Mention>(sentenceRequired.getMentions(MentionType.Required));
			List<Mention> mentionsAllowed = sentenceRequired.getMentions(MentionType.Allowed);
			List<Mention> mentionsFound = sentenceFound.getMentions(MentionType.Found);
			for (Mention mention : mentionsFound) {
				boolean found = false;
				// String sentenceTag = mention.getSentence().getId().getId();
				if (mentionsNotFound.contains(mention)) {
					// System.out.println("TP|" + sentenceTag + "|" +
					// mention.getText() + "|" + mention.getText());
					mentionsNotFound.remove(mention);
					found = true;
					overall.tp++;
					getMentionPerformanceData(mention.getEntityType()).tp++;
					getTextPerformanceData(mention.getText()).tp++;
				} else if (mentionsAllowed.contains(mention)) {
					found = true;
					for (Mention mentionRequired : new HashSet<Mention>(mentionsNotFound)) {
						if (mention.overlaps(mentionRequired)) {
							mentionsNotFound.remove(mentionRequired);
							// System.out.println("*TP|" + sentenceTag + "|" +
							// mentionRequired.getText() + "|"
							// + mention.getText());
							overall.tp++;
							getMentionPerformanceData(mentionRequired.getEntityType()).tp++;
							getTextPerformanceData(mentionRequired.getText()).tp++;
						}
					}
				}
				if (!found) {
					// System.out.println("FP|" + sentenceTag + "|" +
					// mention.getText());
					overall.fp++;
					getMentionPerformanceData(mention.getEntityType()).fp++;
					getTextPerformanceData(mention.getText()).fp++;
				}
			}
			for (Mention mentionNotFound : mentionsNotFound) {
				// String sentenceTag =
				// mentionNotFound.getSentence().getId().getId();
				// System.out.println("FN|" + sentenceTag + "|" +
				// mentionNotFound.getText());
				overall.fn++;
				getMentionPerformanceData(mentionNotFound.getEntityType()).fn++;
				getTextPerformanceData(mentionNotFound.getText()).fn++;
			}
		}

		public PerformanceData getOverall() {
			return overall;
		}

		public Map<EntityType, PerformanceData> getPerMention() {
			return Collections.unmodifiableMap(perMention);
		}

		public Map<String, PerformanceData> getPerText() {
			return Collections.unmodifiableMap(perText);
		}

		public void print() {
			System.out.println("OVERALL: ");
			overall.print();
			for (EntityType type : perMention.keySet()) {
				System.out.println();
				System.out.println("TYPE: \"" + type.getText() + "\"");
				perMention.get(type).print();
			}
			// TODO Make per-type configurable
			// for (String text : perText.keySet())
			// {
			// PerformanceData performanceData = perText.get(text);
			// if (performanceData.fn > performanceData.tp || performanceData.fp > performanceData.tp)
			// {
			// System.out.println();
			// System.out.println("TEXT: \"" + text + "\"");
			// performanceData.print();
			// }
			// }
		}

	}

	public static class PerformanceData {
		int tp;
		int fp;
		int fn;

		public PerformanceData() {
			tp = 0;
			fp = 0;
			fn = 0;
		}

		public double getPrecision() {
			return (double) tp / (tp + fp);
		}

		public double getRecall() {
			return (double) tp / (tp + fn);
		}

		public double getFMeasure() {
			double p = getPrecision();
			double r = getRecall();
			return 2.0 * p * r / (p + r);
		}

		public void print() {
			System.out.println("TP: " + tp);
			System.out.println("FP: " + fp);
			System.out.println("FN: " + fn);
			System.out.println("precision: " + getPrecision());
			System.out.println("   recall: " + getRecall());
			System.out.println("f-measure: " + getFMeasure());
		}
	}

	private static void outputAnalysis(Sentence sentenceRequired, Sentence sentenceFound, PrintWriter mentionOutputFile, boolean outputIfCorrect) {
		Sentence sentenceRequired2 = sentenceRequired.copy(true, true);
		FlattenPostProcessor fpp = new FlattenPostProcessor(FlattenType.Union);
		fpp.postProcess(sentenceRequired2);
		List<Mention> mentionsAllowed = sentenceRequired2.getMentions(MentionType.Allowed);
		Set<Mention> mentionsFoundCorrect = new HashSet<Mention>();
		Set<Mention> mentionsFoundIncorrect = new HashSet<Mention>();
		Set<Mention> mentionsNotFound = new HashSet<Mention>();
		mentionsNotFound.addAll(sentenceRequired2.getMentions(MentionType.Required));
		for (Mention mention : sentenceFound.getMentions(MentionType.Required)) {
			boolean found = false;
			if (mentionsNotFound.contains(mention)) {
				mentionsNotFound.remove(mention);
				mentionsFoundCorrect.add(mention);
				found = true;
			} else if (mentionsAllowed.contains(mention)) {
				mentionsFoundCorrect.add(mention);
				found = true;
				for (Mention mentionRequired : new HashSet<Mention>(mentionsNotFound))
					if (mention.overlaps(mentionRequired))
						mentionsNotFound.remove(mentionRequired);
			}
			if (!found)
				mentionsFoundIncorrect.add(mention);
		}

		// Need to handle five cases:
		// --------------------------
		// Token is not part of any mention
		// Token is part of a mention from mentionsFoundCorrect
		// Token is part of a mention from mentionsFoundIncorrect
		// Token is part of a mention from mentionsNotFound
		// Token is part of a mention from BOTH mentionsFoundIncorrect and
		// mentionsNotFound

		boolean foundError = false;
		StringBuffer analysis = new StringBuffer(sentenceFound.getSentenceId());
		FontColor currentColor = FontColor.Black;
		List<Token> tokens = sentenceFound.getTokens();
		for (int i = 0; i < tokens.size(); i++) {
			boolean inFoundCorrect = false;
			for (Mention mention : mentionsFoundCorrect)
				inFoundCorrect |= mention.contains(i);
			boolean inFoundIncorrect = false;
			for (Mention mention : mentionsFoundIncorrect)
				inFoundIncorrect |= mention.contains(i);
			boolean inNotFound = false;
			for (Mention mention : mentionsNotFound)
				inNotFound |= mention.contains(i);
			foundError |= inNotFound || inFoundIncorrect;
			if (inFoundCorrect) {
				if (inFoundIncorrect || inNotFound) {
					System.out.println("=============");
					System.out.println("inFoundIncorrect: " + inFoundIncorrect);
					System.out.println("inNotFound: " + inNotFound);
					System.out.println(sentenceFound.getSentenceId());
					System.out.println(sentenceFound.getText());
					Mention badMention = sentenceFound.getMentions(tokens.get(i), EnumSet.of(MentionType.Required)).get(0);
					System.out.println("badMention: " + badMention);
					System.out.println("sentenceFound.getMentions().contains(): " + sentenceFound.getMentions(MentionType.Required).contains(badMention));
					System.out.println("mentionsRequired.contains(): " + sentenceRequired.getMentions(MentionType.Required).contains(badMention));
					System.out.println("mentionsAllowed.contains(): " + mentionsAllowed.contains(badMention));
					System.out.println("mentionsFoundCorrect.contains(): " + mentionsFoundCorrect.contains(badMention));
					System.out.println("mentionsFoundIncorrect.contains(): " + mentionsFoundIncorrect.contains(badMention));
					System.out.println("mentionsNotFound.contains(): " + mentionsNotFound.contains(badMention));
					System.out.println("sentenceFound.getMentions(): " + sentenceFound.getMentions(MentionType.Required));
					System.out.println("mentionsFoundCorrect: " + mentionsFoundCorrect);
					System.out.println("mentionsFoundIncorrect: " + mentionsFoundIncorrect);
					System.out.println("mentionsNotFound: " + mentionsNotFound);
					System.out.println("=============");
				}
				assert !inFoundIncorrect;
				assert !inNotFound;
				analysis.append(currentColor.changeColor(FontColor.Green));
				currentColor = FontColor.Green;
			} else if (inFoundIncorrect && inNotFound) {
				analysis.append(currentColor.changeColor(FontColor.Purple));
				currentColor = FontColor.Purple;
			} else if (inFoundIncorrect) {
				analysis.append(currentColor.changeColor(FontColor.Red));
				currentColor = FontColor.Red;
			} else if (inNotFound) {
				analysis.append(currentColor.changeColor(FontColor.Blue));
				currentColor = FontColor.Blue;
			} else {
				analysis.append(currentColor.changeColor(FontColor.Black));
				currentColor = FontColor.Black;
			}
			analysis.append(tokens.get(i).getText());
		}
		analysis.append(currentColor.changeColor(FontColor.Black));
		analysis.append("<br>");
		if (foundError || outputIfCorrect)
			mentionOutputFile.println(analysis);
	}

	public static Dataset getDataset(HierarchicalConfiguration config) {
		Tokenizer tokenizer = getTokenizer(config);
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String datasetName = localConfig.getString("datasetName");
		Dataset dataset = null;
		try {
			dataset = (Dataset) Class.forName(datasetName).newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		dataset.setTokenizer(tokenizer);
		dataset.load(config);
		return dataset;
	}

	private static TagFormat getTagFormat(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		return TagFormat.valueOf(localConfig.getString("tagFormat"));
	}

	public static Tokenizer getTokenizer(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		try {
			String tokenizerName = localConfig.getString("tokenizer");
			Tokenizer tokenizer = (Tokenizer) Class.forName(tokenizerName).newInstance();
			return tokenizer;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static DictionaryTagger getDictionary(HierarchicalConfiguration config) {
		Tokenizer tokenizer = getTokenizer(config);
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String dictionaryName = localConfig.getString("dictionaryTagger");
		if (dictionaryName == null)
			return null;
		DictionaryTagger dictionary = null;
		try {
			dictionary = (DictionaryTagger) Class.forName(dictionaryName).newInstance();
			dictionary.configure(config, tokenizer);
			dictionary.load(config);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return dictionary;
	}

	public static PostProcessor getPostProcessor(HierarchicalConfiguration config) {
		// Guaranteed not to be null
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		SequentialPostProcessor postProcessor = new SequentialPostProcessor();
		if (localConfig.containsKey("useParenthesisPostProcessing"))
			if (localConfig.getBoolean("useParenthesisPostProcessing"))
				postProcessor.addPostProcessor(new ParenthesisPostProcessor());
		if (localConfig.containsKey("useLocalAbbreviationPostProcessing"))
			if (localConfig.getBoolean("useLocalAbbreviationPostProcessing"))
				postProcessor.addPostProcessor(new LocalAbbreviationPostProcessor());
		return postProcessor;
	}

	private static int getCRFOrder(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		return localConfig.getInt("crfOrder");
	}

	public static dragon.nlp.tool.Tagger getPosTagger(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());

		String posTagger = localConfig.getString("posTagger");
		if (posTagger == null)
			return null;
		String posTaggerDataDirectory = localConfig.getString("posTaggerDataDirectory");
		if (posTaggerDataDirectory == null)
			throw new IllegalArgumentException("Must specify data directory for POS tagger");

		if (posTagger.equals(HeppleTagger.class.getName()))
			return new HeppleTagger(posTaggerDataDirectory);
		else if (posTagger.equals(MedPostTagger.class.getName()))
			return new MedPostTagger(posTaggerDataDirectory);
		else
			throw new IllegalArgumentException("Unknown POS tagger type: " + posTagger);
	}

	public static EngLemmatiser getLemmatiser(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String lemmatiserDataDirectory = localConfig.getString("lemmatiserDataDirectory");
		if (lemmatiserDataDirectory == null)
			return null;
		return new EngLemmatiser(lemmatiserDataDirectory, false, true);
	}

	public static String getSimFindFilename(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String simFindFilename = localConfig.getString("simFindFilename");
		return simFindFilename;
	}

	private static Set<MentionType> getMentionTypes(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String mentionTypesStr = localConfig.getString("mentionTypes");
		if (mentionTypesStr == null)
			throw new RuntimeException("Configuration must contain parameter \"mentionTypes\"");
		Set<MentionType> mentionTypes = new HashSet<MentionType>();
		for (String mentionTypeName : mentionTypesStr.split("\\s+"))
			mentionTypes.add(MentionType.valueOf(mentionTypeName));
		return EnumSet.copyOf(mentionTypes);
	}

	private static OverlapOption getSameTypeOverlapOption(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String sameTypeOverlapOption = localConfig.getString("sameTypeOverlapOption");
		if (sameTypeOverlapOption == null)
			throw new RuntimeException("Configuration must contain parameter \"sameTypeOverlapOption\"");
		return OverlapOption.valueOf(sameTypeOverlapOption);
	}

	private static OverlapOption getDifferentTypeOverlapOption(HierarchicalConfiguration config) {
		HierarchicalConfiguration localConfig = config.configurationAt(BANNER.class.getPackage().getName());
		String differentTypeOverlapOption = localConfig.getString("differentTypeOverlapOption");
		if (differentTypeOverlapOption == null)
			throw new RuntimeException("Configuration must contain parameter \"differentTypeOverlapOption\"");
		return OverlapOption.valueOf(differentTypeOverlapOption);
	}
}
