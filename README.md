

#Concept of my algorithm
Like bagging method, my algorithm randomly generates many training sets from the given training set and
 creates predictors ("taggers" in this problem) using training sets. After filtering the taggers by some criteria, each remaining tagger generates result. Results were aggregated with voting to create final result.

This algorithm may have following advantages. (I don't know and didn't tested which one actually effects.)

* Freedom of selecting threshold for final vote enables me to create final result that has the best balance of precision and recall.
* Voting by many taggers may reduce variance and improve stability.
* The filtering of taggers may improve precision.

#What I tried and why
First, after cloning the code from repository I run the BANNER program using expert training file only to get example result.
I thought there was something unbalanced in precision vs recall. Then I decided to use my algorithm, that is, the most simple one among the algorithms that enable me to change the size of final result.

To implement my algorithm, mainly I modified following codes.

* crowd_words/src/org/scripps/crowdwords/TestAggregation.java
    - main
    - executeVotingExperiment
* banner_source/src/banner/eval/BANNER.java
    - train (some codes are extracted to createTagger method)
* banner_source/src/BANNER_BioC.java
    - main
    - BANNER_BioC
    - processPassage

#How my code works
##1. crowd_words/src/org/scripps/crowdwords/TestAggregation
In main method, repeatedly executeVotingExperiment method is called with some parameters(percentage of documents used, percentage of annotator used, threshold of voting) changing.

In executeVotingExperiment method, documents and annotators are sampled from "data/mturk/newpubmed_e12_13_bioc.xml" with some parameters. Simple vote process by sampled annotators is applied to sampled documents and create new training xml file.
##2. banner_source/src/banner/eval/BANNER
In main method, the train method is called.

In train method, the training xml files created by TestAggregation.java are read. Each file is concatenated with the training set that is randomly sampled from "config/banner_bioc.xml" and used for creating tagger by createTagger method.

In createTagger method, tagger is created by CRFTagger.train method and tested with out of bag sample. Then each tagger is saved as a file whose filename contains the score(all of precision, recall, f-score) of the out of bag test.
##3. banner_source/src/BANNER_BioC
In main method, the BANNER_BioC instance is created and then processFile method is called.

In BANNER_BioC constructor, called with parameters(which score is used as criterion of filtering, minimum score value), tagger files whose score meets the condition of the parameters are loaded and a list of taggers is created.

In processFile method, for each sentence in testing file is processed by processPassage method and submission file is created.

In processPassage method, called with parameter(threshold of vote), each tagger in list of taggers makes annotations for the passage and returns result by using simple vote process to the annotations.

#Determination of parameters
* Values of parameters for TestAggregation are determined by my intuition and a little trial and error.
* Values of parameters for BANNER_BioC are determined by trial and error (commented out part in main method of BANNER_BioC).

#Steps for using my program
1. Compile and run crowd_words/src/org/scripps/crowdwords/TestAggregation with no parameters. Then training files will be created at crowd_words/train.
2. Move training files to banner_source/train.
3. Compile and run banner_source/src/banner/eval/BANNER with no parameters.
(In this step, my program stopped twice because of battery problem of my PC. So, the difference of results may be little, if you want to completely repeat my operation please stop the program after 33rd (lexicographical order of filename) training and delete first 33 training files. Then modify line 60 of the source to 'Random rnd = new Random(1);' and again compile it and run. After first 35 training,  stop the program and goto next step.)
4. Compile and run banner_source/src/BANNER_BioC with no parameters.
5. BannerAnnotate.java will be created at banner_source folder.