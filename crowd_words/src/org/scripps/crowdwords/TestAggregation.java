/**
 * 
 */
package org.scripps.crowdwords;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

import javax.xml.stream.XMLStreamException;

import bioc.BioCAnnotation;
import bioc.BioCCollection;
import bioc.BioCDocument;
import bioc.BioCLocation;
import bioc.BioCPassage;
import bioc.io.BioCCollectionReader;
import bioc.io.BioCDocumentWriter;
import bioc.io.BioCFactory;

/**
 * @author bgood
 *
 */
public class TestAggregation {

	/**
     *
	 * @param args
	 * @throws XMLStreamException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws XMLStreamException, IOException {
        //repeatedly executeVotingExperiment method is called with some parameters(percentage of documents used, percentage of annotator used, threshold of voting) changing.

        String mturkfile = "data/mturk/newpubmed_e12_13_bioc.xml";
        String k_dir = "train/";
        Random rnd = new Random(0);
        for (int i = 20; i <= 80; i += 20) {
            for (int j = 40; j <= 80; j += 20) {
                for (int k = 0; k < 3; k++) {
                    executeVotingExperiment(mturkfile, k_dir, 0.01*i, 0.01*j, rnd);
                }
            }
        }
    }


	public static void executeVotingExperiment(String mturkfile, String k_dir, double parcentageValidUser, double parcentageDocuments, Random rnd) throws XMLStreamException, IOException{
		//documents and annotators are sampled from "data/mturk/newpubmed_e12_13_bioc.xml" with some parameters.
		//Simple vote process by sampled annotators is applied to sampled documents and create new training xml file.

        AnnotationComparison ac = new AnnotationComparison();

		//read in the mturk annotations
		BioCCollection mturk_collection_all = readBioC(mturkfile);
        BioCCollection mturk_collection = new BioCCollection();
        for(BioCDocument doc : mturk_collection_all.getDocuments()) {
            if(rnd.nextDouble() < parcentageDocuments) mturk_collection.addDocument(doc);
        }

		//get the full text for export later
		Map<String, Document> id_doc = convertBioCCollectionToDocMap(mturk_collection);
		//convert to local annotation representation
		List<Annotation> mturk_annos = convertBioCtoAnnotationList(mturk_collection);
        Set<String> allUsers = new HashSet<String>();
        for(Annotation anno : mturk_annos) allUsers.add("" + anno.getUser_id());
        Set<String> validUsers = new HashSet<String>();
        for(String user : allUsers) if(rnd.nextDouble() < parcentageValidUser) validUsers.add(user);
		//build different k (voting) thresholds for annotations from turkers
		Aggregator agg = new Aggregator();
		Map<Integer, List<Annotation>> k_annos = agg.getAnnotationMapByK(mturk_annos, validUsers);
        int rndInt = Math.abs(rnd.nextInt());
		for(Integer k=2; k <= 11; k+=3){// : k_annos.keySet()){
			List<Annotation> annos = k_annos.get(k);
            if(annos == null || annos.size() < mturk_annos.size() / allUsers.size() / 2) continue;
			//export a BioC version for banner
			BioCCollection k_collection = convertAnnotationsToBioC(annos, id_doc,"mturk k="+k,"",1000000);
			writeBioC(k_collection, k_dir+ (int)(parcentageValidUser * 100) + "-" + (int)(parcentageDocuments * 100) + "-" + k + "-" + rndInt +".xml");
		}
	}
	
	public static BioCCollection readBioC(String file) throws FileNotFoundException, XMLStreamException{
		BioCFactory factory = BioCFactory.newFactory(BioCFactory.WOODSTOX);
		BioCCollectionReader reader =
				factory.createBioCCollectionReader(new FileReader(file));    
		BioCCollection collection = reader.readCollection();
		return collection;
	}

	public static void writeBioC(BioCCollection collection, String file) throws XMLStreamException, IOException{
		OutputStream out = new FileOutputStream(file);
		BioCFactory factory = BioCFactory.newFactory(BioCFactory.STANDARD);
		BioCDocumentWriter writer =
				factory.createBioCDocumentWriter(new OutputStreamWriter(out));
		writer.writeCollectionInfo(collection);
		for ( BioCDocument document : collection ) {
			writer.writeDocument(document);
		}
		writer.close();
	}

	public static List<Annotation> convertBioCtoAnnotationList(BioCCollection biocCollection){
		List<Annotation> annos = new ArrayList<Annotation>();
		for(BioCDocument doc : biocCollection.getDocuments()){
			Integer pmid = Integer.parseInt(doc.getID());
			//			String n_annotators = doc.getInfon("n_annotators");
			//			String annotator_ids = doc.getInfon("annotators");
			for(BioCPassage passage : doc.getPassages()){
				String type = passage.getInfon("type");
				if(type.equals("title")){
					type = "t";
				}else if(type.equals("abstract")){
					type = "a";
				}
				for(BioCAnnotation bca : passage.getAnnotations()){
					//assumes that we have one location per annotation
					//will work until we get to relations.
					int offset = bca.getLocations().get(0).getOffset();
					Annotation anno = new Annotation(bca.getText(), offset, offset+bca.getText().length(), pmid, type, "loc");
					String annotatorId = bca.getInfon("annotator_id");
					anno.setUser_id(Integer.parseInt(annotatorId));
					anno.setId(Integer.parseInt(bca.getID()));
					annos.add(anno);
				}
			}
		}
		return annos;
	}

	public static BioCCollection convertAnnotationsToBioC(List<Annotation> annos, Map<String, Document> docid_doc, String source, String date, int limit) throws IOException{
		//create the BioC collection
		BioCCollection biocCollection = new BioCCollection();
		biocCollection.setSource(source);
		biocCollection.setDate(date);

		SentenceSplitter splitter = new SentenceSplitter();
		//this turns the list of annotations into a map keyed by pmid
		AnnotationComparison annocompare = new AnnotationComparison();
		Map<Integer, List<Annotation>> docid_annotations = annocompare.listtomaplist(annos);
		int fine = 0; int ndocs = 0;
		for(Integer docid : docid_annotations.keySet()){
			ndocs++;
			Document doc = docid_doc.get(docid+"");
			//create bioc doc
			BioCDocument biocDoc = new BioCDocument();
			biocDoc.setID(docid+"");
			//make two passages per doc - one for the title and one for the abstract
			BioCPassage title_passage = new BioCPassage();
			title_passage.setOffset(0);
			title_passage.setText(doc.getTitle()+" "); //TODO will need to make some checks and adjustments to make sure that the pffsets of the annotations are exactly right
			title_passage.putInfon("type", "title");
			BioCPassage abstract_passage = new BioCPassage();
			abstract_passage.setOffset(doc.getTitle().length()+1);
			abstract_passage.setText(doc.getText());
			abstract_passage.putInfon("type", "abstract");
			//get the annotations for this document
			List<Annotation> dannos = docid_annotations.get(docid);
			//put them in order
			Collections.sort(dannos);
			//create the BioC versions			
			Set<String> workers = new HashSet<String>();
			for(Annotation anno : dannos){		
				List<String> annospecific_workers = null;
				if(anno.getUser_id()==0){
					annospecific_workers = anno.getAnnotators();
					if(annospecific_workers!=null){
						for(String w : annospecific_workers){
							workers.add(w);
						}
					}
				}else{
					workers.add(anno.getUser_id()+"");
				}
				int stop = anno.getStop(); int start = anno.getStart();
				String kind = "a";

				if(anno.getDocument_section().equals("t")){
					kind = "t";
				}else if(anno.getDocument_section().equals("a")){
					start++;
				}				
				//check if valid
				String tmp = title_passage.getText();
				//uncomment to make the offsets relative to the passage, not the document
				//if(kind.equals("a")){
				//	tmp = abstract_passage.getText();
				//start = start - title_passage.getText().length();
				//}
				//comment this if you want the offsets relative to the passage
				tmp = tmp+abstract_passage.getText();

				//set the annotation start and stops
				stop = start+anno.getText().length();

				//don't export if not valid
				if(start>-1&&!anno.getText().equals(tmp.substring(start, stop))){
					System.out.println("Boundary error, skipping: "+anno.getText()+"\t"+tmp.substring(start, stop));
				}else if(start==stop){
					System.out.println("Empty annotation, skipping");
				}else{//all ok
					fine++;
					//create the bioc annotation
					BioCAnnotation biocAnno = new BioCAnnotation();
					biocAnno.setID(anno.getId()+"");
					biocAnno.putInfon("type", "Disease");
					BioCLocation loc = new BioCLocation();
					loc.setLength(stop-start);
					loc.setOffset(start);					
					biocAnno.addLocation(loc);
					biocAnno.setText(anno.getText()); 
					//if multiple annotator..
					if(anno.getUser_id()==0){
						if(annospecific_workers!=null){
							biocAnno.putInfon("annotator_ids", annospecific_workers.toString());
							biocAnno.putInfon("n_annotators", annospecific_workers.size()+"");
						}
					}else{
						biocAnno.putInfon("annotator_id",anno.getUser_id()+"");
					}
					//add the annotation to the passage
					if(kind.equals("t")){
						title_passage.addAnnotation(biocAnno);
					}else{	
						abstract_passage.addAnnotation(biocAnno);
					}
				}

			}
			biocDoc.putInfon("n_annotators",workers.size()+"");
			biocDoc.putInfon("annotator_ids", workers.toString());

			//add these passages to the doc
			biocDoc.addPassage(title_passage);
			biocDoc.addPassage(abstract_passage);
			//add the doc to the collection
			biocCollection.addDocument(biocDoc);
			if(ndocs>limit){
				break;
			}
		}
		return biocCollection;
	}

	public static Map<String, Document> convertBioCCollectionToDocMap(BioCCollection biocCollection){
		Map<String, Document> id_doc = new HashMap<String, Document>();
		for(BioCDocument biodoc : biocCollection.getDocuments()){
			Integer pmid = Integer.parseInt(biodoc.getID());
			Document doc = new Document();
			doc.setId(pmid);
			for(BioCPassage passage : biodoc.getPassages()){
				String type = passage.getInfon("type");
				if(type.equals("abstract")){
					doc.setText(passage.getText());
				}else if(type.equals("title")){
					doc.setTitle(passage.getText());
				}
			}
			id_doc.put(pmid+"", doc);
		}	
		return id_doc;
	}

}
